/*
  ==============================================================================

   This file is part of the JUCE tutorials.
   Copyright (c) 2020 - Raw Material Software Limited

   The code included in this file is provided under the terms of the ISC license
   http://www.isc.org/downloads/software-support-policy/isc-license. Permission
   To use, copy, modify, and/or distribute this software for any purpose with or
   without fee is hereby granted provided that the above copyright notice and
   this permission notice appear in all copies.

   THE SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY, AND ALL WARRANTIES,
   WHETHER EXPRESSED OR IMPLIED, INCLUDING MERCHANTABILITY AND FITNESS FOR
   PURPOSE, ARE DISCLAIMED.

  ==============================================================================
*/

/*******************************************************************************
 The block below describes the properties of this PIP. A PIP is a short snippet
 of code that can be read by the Projucer and used to generate a JUCE project.

 BEGIN_JUCE_PIP_METADATA

 name:             AudioProcessorGraphTutorial
 version:          1.0.0
 vendor:           JUCE
 website:          http://juce.com
 description:      Explores the audio processor graph.

 dependencies:     juce_audio_basics, juce_audio_devices, juce_audio_formats,
                   juce_audio_plugin_client, juce_audio_processors,
                   juce_audio_utils, juce_core, juce_data_structures, juce_dsp,
                   juce_events, juce_graphics, juce_gui_basics, juce_gui_extra
 exporters:        xcode_mac, vs2019, linux_make

 type:             AudioProcessor
 mainClass:        TutorialProcessor

 useLocalCopy:     1

 END_JUCE_PIP_METADATA

*******************************************************************************/


#pragma once

//==============================================================================
class ProcessorBase  : public juce::AudioProcessor
{
public:
    //==============================================================================
    ProcessorBase()
        : AudioProcessor (BusesProperties().withInput ("Input", juce::AudioChannelSet::stereo())
                                           .withOutput ("Output", juce::AudioChannelSet::stereo()))
    {}

    //==============================================================================
    void prepareToPlay (double, int) override {}
    void releaseResources() override {}
    void processBlock (juce::AudioSampleBuffer&, juce::MidiBuffer&) override {}

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override          { return nullptr; }
    bool hasEditor() const override                              { return false; }

    //==============================================================================
    const juce::String getName() const override                  { return {}; }
    bool acceptsMidi() const override                            { return false; }
    bool producesMidi() const override                           { return false; }
    double getTailLengthSeconds() const override                 { return 0; }

    //==============================================================================
    int getNumPrograms() override                                { return 0; }
    int getCurrentProgram() override                             { return 0; }
    void setCurrentProgram (int) override                        {}
    const juce::String getProgramName (int) override             { return {}; }
    void changeProgramName (int, const juce::String&) override   {}

    //==============================================================================
    void getStateInformation (juce::MemoryBlock&) override       {}
    void setStateInformation (const void*, int) override         {}

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (ProcessorBase)
};

// GSA : AudioProcessor
// Limiter : AudioProcessor
// Graph = AudioGraphProcessor : AudioProcessor
// Graph.add(Filter) & Graph.Connection(Filter) -> Graph.processBlock()
 
// Act.Item 1) 다음주 목금 사이에 현재 사이에 사용하고있는 필터별 스펙 & Config. 정리해놓기
// Act.Item 2) 직접듣는게 아니라 Visualize Tool로 디버깅 ...


//==============================================================================
class OscillatorProcessor  : public ProcessorBase
{
public:
    OscillatorProcessor()
    {
        oscillator.setFrequency (2000.0f);
        oscillator.initialise ([] (float x) { return std::sin (x); });
    }

    void prepareToPlay (double sampleRate, int samplesPerBlock) override
    {
        juce::dsp::ProcessSpec spec { sampleRate, static_cast<juce::uint32> (samplesPerBlock) };
        oscillator.prepare (spec);
    }

    void processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer&) override
    {
        juce::dsp::AudioBlock<float> block (buffer);
        juce::dsp::ProcessContextReplacing<float> context (block);
        oscillator.process (context); // Check This!!
    }

    void reset() override
    {
        oscillator.reset();
    }

    const juce::String getName() const override { return "Oscillator"; }

private:
    juce::dsp::Oscillator<float> oscillator;
};

//==============================================================================
class GainProcessor  : public ProcessorBase
{
public:
    GainProcessor()
    {
        gain.setGainDecibels (-6.0f);
    }

    void prepareToPlay (double sampleRate, int samplesPerBlock) override
    {
        juce::dsp::ProcessSpec spec { sampleRate, static_cast<juce::uint32> (samplesPerBlock), 2 };
        gain.prepare (spec);
    }

    void processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer&) override
    {
        juce::dsp::AudioBlock<float> block (buffer);
        juce::dsp::ProcessContextReplacing<float> context (block);
        gain.process (context);
    }

    void reset() override
    {
        gain.reset();
    }

    const juce::String getName() const override { return "Gain"; }

private:
    juce::dsp::Gain<float> gain;
};

//==============================================================================
class FilterProcessor  : public ProcessorBase
{
public:
    FilterProcessor() {}

    void prepareToPlay (double sampleRate, int samplesPerBlock) override
    {
        *filter.state = *juce::dsp::IIR::Coefficients<float>::makeHighPass (sampleRate, 1000.0f);

        juce::dsp::ProcessSpec spec { sampleRate, static_cast<juce::uint32> (samplesPerBlock), 2 };
        filter.prepare (spec);
    }

    void processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer&) override
    {
        juce::dsp::AudioBlock<float> block (buffer);
        juce::dsp::ProcessContextReplacing<float> context (block);
        filter.process (context);
    }

    void reset() override
    {
        filter.reset();
    }

    const juce::String getName() const override { return "Filter"; }

private:
    juce::dsp::ProcessorDuplicator<juce::dsp::IIR::Filter<float>, juce::dsp::IIR::Coefficients<float>> filter;
};

//==============================================================================
class TutorialProcessor  : public juce::AudioProcessor
{
public:
    //==============================================================================
    using AudioGraphIOProcessor = juce::AudioProcessorGraph::AudioGraphIOProcessor;
    using Node = juce::AudioProcessorGraph::Node;

    //==============================================================================
    TutorialProcessor()
        : AudioProcessor (BusesProperties().withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                                           .withOutput ("Output", juce::AudioChannelSet::stereo(), true)),
          mainProcessor  (new juce::AudioProcessorGraph()),
          muteInput      (new juce::AudioParameterBool   ("mute",    "Mute Input", true)),
          processorSlot1 (new juce::AudioParameterChoice ("slot1",   "Slot 1",     processorChoices, 0)),
          processorSlot2 (new juce::AudioParameterChoice ("slot2",   "Slot 2",     processorChoices, 0)),
          processorSlot3 (new juce::AudioParameterChoice ("slot3",   "Slot 3",     processorChoices, 0)),
          processorSlot1Para (new juce::AudioParameterChoice ("slot1Para",   "Slot 1 Para",     processorChoices, 0)),
          processorSlot2Para (new juce::AudioParameterChoice ("slot2Para",   "Slot 2 Para",     processorChoices, 0)),
          processorSlot3Para (new juce::AudioParameterChoice ("slot3Para",   "Slot 3 Para",     processorChoices, 0)),
          bypassSlot1    (new juce::AudioParameterBool   ("bypass1", "Bypass 1",   false)),
          bypassSlot2    (new juce::AudioParameterBool   ("bypass2", "Bypass 2",   false)),
          bypassSlot3    (new juce::AudioParameterBool   ("bypass3", "Bypass 3",   false)),
          bypassSlot1Para    (new juce::AudioParameterBool   ("bypass1Para", "Bypass 1 Para",   false)),
          bypassSlot2Para    (new juce::AudioParameterBool   ("bypass2Para", "Bypass 2 Para",   false)),
          bypassSlot3Para    (new juce::AudioParameterBool   ("bypass3Para", "Bypass 3 Para",   false))
    {
        addParameter (muteInput);

        addParameter (processorSlot1);
        addParameter (processorSlot2);
        addParameter (processorSlot3);
        addParameter (processorSlot1Para);
        addParameter (processorSlot2Para);
        addParameter (processorSlot3Para);

        addParameter (bypassSlot1);
        addParameter (bypassSlot2);
        addParameter (bypassSlot3);
        addParameter (bypassSlot1Para);
        addParameter (bypassSlot2Para);
        addParameter (bypassSlot3Para);
    }

    //==============================================================================
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override
    {
        if (layouts.getMainInputChannelSet()  == juce::AudioChannelSet::disabled()
         || layouts.getMainOutputChannelSet() == juce::AudioChannelSet::disabled())
            return false;

        if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
         && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
            return false;

        return layouts.getMainInputChannelSet() == layouts.getMainOutputChannelSet();
    }

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override
    {
        mainProcessor->setPlayConfigDetails (getMainBusNumInputChannels(),
                                             getMainBusNumOutputChannels(),
                                             sampleRate, samplesPerBlock);

        mainProcessor->prepareToPlay (sampleRate, samplesPerBlock);

        initialiseGraph();
    }

    void releaseResources() override
    {
        mainProcessor->releaseResources();
    }

    void processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer& midiMessages) override
    {
        for (int i = getTotalNumInputChannels(); i < getTotalNumOutputChannels(); ++i)
            buffer.clear (i, 0, buffer.getNumSamples());

        updateGraph();

        mainProcessor->processBlock (buffer, midiMessages);
    }

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override          { return new juce::GenericAudioProcessorEditor (*this); }
    bool hasEditor() const override                              { return true; }

    //==============================================================================
    const juce::String getName() const override                  { return "Graph Tutorial"; }
    bool acceptsMidi() const override                            { return true; }
    bool producesMidi() const override                           { return true; }
    double getTailLengthSeconds() const override                 { return 0; }

    //==============================================================================
    int getNumPrograms() override                                { return 1; }
    int getCurrentProgram() override                             { return 0; }
    void setCurrentProgram (int) override                        {}
    const juce::String getProgramName (int) override             { return {}; }
    void changeProgramName (int, const juce::String&) override   {}

    //==============================================================================
    void getStateInformation (juce::MemoryBlock&) override       {}
    void setStateInformation (const void*, int) override         {}

private:
    //==============================================================================
    void initialiseGraph()
    {
        mainProcessor->clear();

        audioInputNode  = mainProcessor->addNode (std::make_unique<AudioGraphIOProcessor> (AudioGraphIOProcessor::audioInputNode));
        audioOutputNode = mainProcessor->addNode (std::make_unique<AudioGraphIOProcessor> (AudioGraphIOProcessor::audioOutputNode));
        midiInputNode   = mainProcessor->addNode (std::make_unique<AudioGraphIOProcessor> (AudioGraphIOProcessor::midiInputNode));
        midiOutputNode  = mainProcessor->addNode (std::make_unique<AudioGraphIOProcessor> (AudioGraphIOProcessor::midiOutputNode));

        connectAudioNodes();
        connectMidiNodes();
    }

    void updateGraph()
    {
        bool hasChanged = false;

        juce::Array<juce::AudioParameterChoice*> choices { processorSlot1,
                                                           processorSlot2,
                                                           processorSlot3,
                                                           processorSlot1Para,
                                                           processorSlot2Para,
                                                           processorSlot3Para };

        juce::Array<juce::AudioParameterBool*> bypasses { bypassSlot1,
                                                          bypassSlot2,
                                                          bypassSlot3,
                                                          bypassSlot1Para,
                                                          bypassSlot2Para,
                                                          bypassSlot3Para };

        juce::ReferenceCountedArray<Node> slots;
        slots.add (slot1Node);
        slots.add (slot2Node);
        slots.add (slot3Node);

        juce::ReferenceCountedArray<Node> slotsPara;
        slotsPara.add (slot1NodePara);
        slotsPara.add (slot2NodePara);
        slotsPara.add (slot3NodePara);

        for (int i = 0; i < 6; ++i)
        {
            auto& choice = choices  .getReference (i); // UI
            auto slot    = i < 3 ? slots.getUnchecked (i) : slotsPara.getUnchecked (i - 3); // Data
            
            if (choice->getIndex() == 0)            // [1] Empty
            {
                if (slot != nullptr)
                {
                    mainProcessor->removeNode (slot.get());
                    if (i < 3)
                        slots.set (i, nullptr);
                    else
                        slotsPara.set (i - 3, nullptr);
                    hasChanged = true;
                }
            }
            else if (choice->getIndex() == 1)       // [2] Oscillator
            {
                if (slot != nullptr)
                {
                    if (slot->getProcessor()->getName() == "Oscillator")
                        continue;

                    mainProcessor->removeNode (slot.get());
                }

                if (i < 3)
                    slots.set (i, mainProcessor->addNode (std::make_unique<OscillatorProcessor>()));
                else
                    slotsPara.set (i - 3, mainProcessor->addNode (std::make_unique<OscillatorProcessor>()));
                hasChanged = true;
            }
            else if (choice->getIndex() == 2)       // [3] Gain
            {
                if (slot != nullptr)
                {
                    if (slot->getProcessor()->getName() == "Gain")
                        continue;

                    mainProcessor->removeNode (slot.get());
                }

                if (i < 3)
                    slots.set (i, mainProcessor->addNode (std::make_unique<GainProcessor>()));
                else
                    slotsPara.set (i - 3, mainProcessor->addNode (std::make_unique<GainProcessor>()));
                hasChanged = true;
            }
            else if (choice->getIndex() == 3)       // [4] Filter
            {
                if (slot != nullptr)
                {
                    if (slot->getProcessor()->getName() == "Filter")
                        continue;

                    mainProcessor->removeNode (slot.get());
                }

                if (i < 3)
                    slots.set (i, mainProcessor->addNode (std::make_unique<FilterProcessor>()));
                else
                    slotsPara.set (i - 3, mainProcessor->addNode (std::make_unique<FilterProcessor>()));
                hasChanged = true;
            }
        }

        if (hasChanged)
        {
            for (auto connection : mainProcessor->getConnections())     // [5]
                mainProcessor->removeConnection (connection);

            juce::ReferenceCountedArray<Node> activeSlots;
            juce::ReferenceCountedArray<Node> activeSlotsPara;

            for (auto slot : slots)
            {
                if (slot != nullptr)
                {
                    activeSlots.add (slot);                             // [6]

                    slot->getProcessor()->setPlayConfigDetails (getMainBusNumInputChannels(),
                                                                getMainBusNumOutputChannels(),
                                                                getSampleRate(), getBlockSize());
                }
            }
            for (auto slot : slotsPara)
            {
                if (slot != nullptr)
                {
                    activeSlotsPara.add (slot);                             // [6]

                    slot->getProcessor()->setPlayConfigDetails (getMainBusNumInputChannels(),
                                                                getMainBusNumOutputChannels(),
                                                                getSampleRate(), getBlockSize());
                }
            }

            if (activeSlots.isEmpty() && activeSlotsPara.isEmpty())                                  // [7]
            {
                connectAudioNodes();
            }
            else
            {
                if (!activeSlots.isEmpty()) {
                    for (int i = 0; i < activeSlots.size() - 1; ++i)        // [8]
                    {
                        for (int channel = 0; channel < 2; ++channel)
                            mainProcessor->addConnection ({ { activeSlots.getUnchecked (i)->nodeID,      channel },
                                                            { activeSlots.getUnchecked (i + 1)->nodeID,  channel } });
                    }

                    for (int channel = 0; channel < 2; ++channel)           // [9] --> Parallel 구조로 변경!
                    {
                        mainProcessor->addConnection ({ { audioInputNode->nodeID,         channel },
                                                        { activeSlots.getFirst()->nodeID, channel } });
                        mainProcessor->addConnection ({ { activeSlots.getLast()->nodeID,  channel },
                                                        { audioOutputNode->nodeID,        channel } });
                    }
                }

                if (!activeSlotsPara.isEmpty()) {
                    for (int i = 0; i < activeSlotsPara.size() - 1; ++i)        // [8]
                    {
                        for (int channel = 0; channel < 2; ++channel)
                            mainProcessor->addConnection ({ { activeSlotsPara.getUnchecked (i)->nodeID,      channel },
                                                            { activeSlotsPara.getUnchecked (i + 1)->nodeID,  channel } });
                    }

                    for (int channel = 0; channel < 2; ++channel)           // [9] --> Parallel 구조로 변경!
                    {
                        mainProcessor->addConnection ({ { audioInputNode->nodeID,         channel },
                                                        { activeSlotsPara.getFirst()->nodeID, channel } });
                        mainProcessor->addConnection ({ { activeSlotsPara.getLast()->nodeID,  channel },
                                                        { audioOutputNode->nodeID,        channel } });
                    }
                }
            }

            connectMidiNodes();

            for (auto node : mainProcessor->getNodes())                 // [10]
                node->getProcessor()->enableAllBuses();
        }

        for (int i = 0; i < 6; ++i)
        {
            auto  slot   = i < 3 ? slots.getUnchecked (i) : slotsPara.getUnchecked (i - 3);
            auto& bypass = bypasses.getReference (i);

            if (slot != nullptr)
                slot->setBypassed (bypass->get());
        }

        audioInputNode->setBypassed (muteInput->get());

        slot1Node = slots.getUnchecked (0);
        slot2Node = slots.getUnchecked (1);
        slot3Node = slots.getUnchecked (2);
        slot1NodePara = slotsPara.getUnchecked (0);
        slot2NodePara = slotsPara.getUnchecked (1);
        slot3NodePara = slotsPara.getUnchecked (2);
    }
    
    void connectAudioNodes()
    {
        for (int channel = 0; channel < 2; ++channel)
            mainProcessor->addConnection ({ { audioInputNode->nodeID,  channel },
                                            { audioOutputNode->nodeID, channel } });
    }

    void connectMidiNodes()
    {
        mainProcessor->addConnection ({ { midiInputNode->nodeID,  juce::AudioProcessorGraph::midiChannelIndex },
                                        { midiOutputNode->nodeID, juce::AudioProcessorGraph::midiChannelIndex } });
    }

    //==============================================================================
    juce::StringArray processorChoices { "Empty", "Oscillator", "Gain", "Filter" };

    std::unique_ptr<juce::AudioProcessorGraph> mainProcessor;

    juce::AudioParameterBool* muteInput;

    juce::AudioParameterChoice* processorSlot1;
    juce::AudioParameterChoice* processorSlot2;
    juce::AudioParameterChoice* processorSlot3;
    juce::AudioParameterChoice* processorSlot1Para;
    juce::AudioParameterChoice* processorSlot2Para;
    juce::AudioParameterChoice* processorSlot3Para;
    
    juce::AudioParameterBool* bypassSlot1;
    juce::AudioParameterBool* bypassSlot2;
    juce::AudioParameterBool* bypassSlot3;
    juce::AudioParameterBool* bypassSlot1Para;
    juce::AudioParameterBool* bypassSlot2Para;
    juce::AudioParameterBool* bypassSlot3Para;

    Node::Ptr audioInputNode;
    Node::Ptr audioOutputNode;
    Node::Ptr midiInputNode;
    Node::Ptr midiOutputNode;

    Node::Ptr slot1Node;
    Node::Ptr slot2Node;
    Node::Ptr slot3Node;
    Node::Ptr slot1NodePara;
    Node::Ptr slot2NodePara;
    Node::Ptr slot3NodePara;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TutorialProcessor)
};
